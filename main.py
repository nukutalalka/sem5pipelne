def sum_even_numbers(numbers):
    even_numbers =  [num for num in numbers if num % 2 == 0]
    return sum(even_numbers)
